from .abstract_clockify import AbstractClockify
import logging
logging.basicConfig(level=logging.INFO)
# Client of project


class Client(AbstractClockify):

    def __init__(self, api_key):
        super(Client, self).__init__(api_key=api_key)

    # returns all client from a workspace
    def get_all_clients(self, workspace_id):
        try:
            logging.info("Start function: get_all_clients")
            url = self.base_url+'workspaces/'+workspace_id+'/clients/'
            return self.request_get(url)
        except Exception as e:
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)
